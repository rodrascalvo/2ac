;
; Search for the maximum of a list of positive integers
;
	.data
list:	.word 6,9,12,92,100,2,3,1 ; list of numbers
count:	.word 8                   ; item count
max:	.space 8                  ; maximum

	.code

main:
	xor r1, r1, r1     ; temporary maximum (0 is the minimum)
	xor r2, r2, r2     ; memory index of the current item
	ld r3, count(r0)   ; remaining items

loop: 
	ld r10, list(r2)   ; next item
	daddi r3, r3, -1   ; count--
	daddui r2, r2, 8   ; point index to next item (64 bits)
	sltu r20, r1, r10  ; unsigned comparison
	movn r1, r10, r20  ; update temporary maximum if greater
	beqz r3, end       ; end of the list
	j loop

end:
	sd r1, max(r0)     ; maximum
	halt
