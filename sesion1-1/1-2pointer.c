#include <stdio.h>

void f(double d)
{
    d = 4;
}

int main(int argc, char* argv[])
{
    double *d;
    double x=3;

    d = &x;

    f(*d);
    printf("%f\n", *d);
    return 0;
}
