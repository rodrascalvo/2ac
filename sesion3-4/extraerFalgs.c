 #include <stdio.h>
#include <atc/linmem.h>

// Given a virtual address, the values of the flags of the page
// are printed on the screen. On top of these, the title passed
// as argument is also displayed.

void print_pte_flags(void *virtual_addr, char *title)
{
    unsigned int pte;
    unsigned int flags_vp;
    const unsigned int FLAG_MASK = 0xFFF;
    const unsigned int PRESENT_BIT_MASK = 1;

    // Print the title
    printf("%s\n", title);

    // Retrieve the entry in the page table for the virtual address
    if (get_pte(virtual_addr, &pte))
    {
        perror("Linmem module error");
        return;
    }

    // Is there PTE?
    if (pte)
    { // OK
        // Store the flags associated at the memory page
        flags_vp = pte & FLAG_MASK;

        ////////
        ///   Print the status of the flags associated to the page
        ////////
        if (flags_vp & PRESENT_BIT_MASK)
            	printf("Present bit = 1\tPage present in memory\n");
        else
            printf("Present bit = 0\tPage is not present in memory\n");

        // Continue with the other flags
    }
    else
    {
        fprintf(stderr, "Page %.5Xh does not have a page table entry\n",
            (unsigned int)virtual_addr >> 12);
    }
}

int main(void)
{
    int local = 0xABCDEF01;

    print_pte_flags(&local, "Local variable\n-----------------");

    printf("\n---- Press [ENTER] to continue");
    getchar();

    return 0;
}
