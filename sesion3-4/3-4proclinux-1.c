// This source file must be compiled with the following command:
//   gcc -Wall 3-4proclinux-1.c 3-4printvm_mod.c -o 3-4proclinux-1 -lmem

#include <stdio.h>

extern void print_virtual_physical_pte( );

int global = 0x12345678;


int main(void)
{
  int local = 0xABCDEF01;

  print_virtual_physical_pte(&global, "Global variable\n"
                                       "------------------\n");
  print_virtual_physical_pte(&local, "Local variable\n"
                                      "-----------------\n");
  print_virtual_physical_pte(print_virtual_physical_pte, "Function\n"
                                                           "----------\n");

  printf("\n---- Press [ENTER] to continue");
  getchar();

  return 0;
}
