int main()
{
  /* Interupt vector address */
  int _far *p = (int _far *)0x0020;

  *p = 0x0000; /* Write offset in interrupt vector */
  p++;
  *p = 0x0000; /* Write segment in interrupt vector */
}
